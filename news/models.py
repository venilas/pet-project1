from django.db import models


class ContentNews(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=255, unique=True)
    line = models.TextField(verbose_name='Контент')
    source = models.CharField(verbose_name='Источник', max_length=255)
    href_source = models.TextField(verbose_name='Ссылка на источники', max_length=255)

    class Meta:
        verbose_name = 'Контент'
        verbose_name_plural = 'Список контентов'

    def __str__(self):
        return self.title


class AvtorNews(models.Model):
    avtor = models.CharField(verbose_name='Автор', unique=True, max_length=255)
    url = models.CharField(verbose_name='Сайт автора', max_length=255)

    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def __str__(self):
        return self.avtor


class News(models.Model):
    avtor = models.ForeignKey(AvtorNews, verbose_name='Автор', on_delete=models.PROTECT, related_name='news')
    title = models.CharField(verbose_name='Заголовок', max_length=255, unique=True)
    content = models.ForeignKey(ContentNews, verbose_name='Контент', null=True, on_delete=models.PROTECT)
    created = models.CharField(verbose_name='Создано', max_length=40)
    photo = models.ImageField(verbose_name='Фото', upload_to='photos/%Y/%m/%d/', blank=True, null=True)
    photo_url = models.TextField(verbose_name='Ссылка на фото', blank=True)
    status = models.BooleanField(verbose_name='Опубликовано', default=0)
    region = models.CharField(verbose_name='Регион', max_length=50)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Список новостей'

    def __str__(self):
        return self.title
