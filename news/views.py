from django.contrib.auth import logout, login
from django.contrib.auth.views import LoginView
from django.views.generic import CreateView
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from news import models
from .forms import *


def NewsDetail(request, pk):
    content_id = models.News.objects.all().get(id=pk).content_id
    data = {
        'title': models.News.objects.all().get(id=pk).title,
        'created': models.News.objects.all().get(id=pk).created,
        'photo': models.News.objects.all().get(id=pk).photo_url,
        'text': models.ContentNews.objects.all().get(id=content_id).line,
    }
    return render(request, 'news/news_detail.html', data)


def main(request):
    data = {
        'title': 'Новости',
        'news': models.News.objects.filter(status=1).order_by('id')[:24],
    }
    return render(request, 'news/index.html', data)


def about(request):
    return render(request, 'news/about.html')


class RegisterUser(CreateView):
    form_class = RegisterUserForm
    template_name = 'news/register.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('main')


class LoginUser(LoginView):
    form_class = LoginUserForm
    template_name = 'news/login.html'

    def get_success_url(self):
        return reverse_lazy('main')


def logout_user(request):
    logout(request)
    return redirect('main')
