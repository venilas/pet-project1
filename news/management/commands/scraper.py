from django.core.management.base import BaseCommand
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

from news import models
from tqdm import tqdm


class Command(BaseCommand):
    def handle(self, *args, **options):
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")  # Выключение графического отображения
        options.add_argument( "user-agent=Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36")
        options.add_argument("--disable-blink-features=AutomationControlled")  # Защита от блокировки от сайта

        driver = webdriver.Chrome(
            service=Service(r'news\parsing\chromedriver.exe'),
            options=options
        )

        url = 'https://dzen.ru/news/region/ufa?issue_tld=ru'
        driver.get(url)

        # Здесь реализовать показ новостей исходя от местоположения

        # url = 'https://dzen.ru/news/region/ufa?issue_tld=ru&modal=regions'
        # ip = узнать айпи посетителя сайта получится после того сервер будет запущен на хосте, а не на локальном сервере
        # city = Используя библиотеку Dadata и узнаём город по ip
        # print(driver.find_element(By.XPATH, '/html/body/div[3]/div[2]/div/div/div/div[1]/div/div[3]/div[17]/a[3]').click()) переход исходя от
        # переменой city и добавить переменную city в строку по которому будет переходить selenium и выдавать данные

        # Нахождение всех карточек с новостями на сайте
        news = driver.find_element(By.XPATH, '//*[@id="neo-page"]/div/div[2]/div[2]/div[1]').find_elements(By.CLASS_NAME, 'mg-grid__col')

        for i in tqdm(news, colour='green'):
            try:
                title = i.find_element(By.CLASS_NAME, 'mg-card__title').text
                href = i.find_element(By.CLASS_NAME, 'mg-card__title').find_element(By.TAG_NAME, 'a').get_attribute('href')
                avtor = i.find_element(By.TAG_NAME, 'span').text
                time = i.find_element(By.CLASS_NAME, 'mg-card-source__time').text

                try:
                    photo = i.find_element(By.CLASS_NAME, 'mg-card__media').find_element(By.TAG_NAME, 'img').get_attribute('src')
                except Exception:
                    photo = i.find_element(By.CLASS_NAME, 'mg-card__media-block_type_image').get_attribute('style').split('"')[1]

                # Добавление авторов в базу данных, try на случай если в БД есть данный автор
                try:
                    models.AvtorNews.objects.create(avtor=avtor, url='в разработке')
                except Exception:
                    pass

                # Берем контент путём открытия в новой вкладке
                driver.execute_script(f"window.open('{href}')")
                driver.switch_to.window(driver.window_handles[-1])
                content = driver.find_element(By.XPATH,'//*[@id="neo-page"]/div/div[2]/div/div[1]/div/div[1]/article/div[2]/div[1]/div/div').find_elements(By.CLASS_NAME, 'mg-snippets-group__item')

                # Добавление текса, источника и ссылку на источника в 3 разных массива
                content_lines = []
                content_source = []
                content_href_source = []

                for lines in content:
                    line = lines.find_element(By.CLASS_NAME, 'mg-snippet__text').text
                    source = lines.find_element(By.CLASS_NAME, 'mg-snippet-source-info__agency-name').text
                    href_source = lines.find_element(By.CLASS_NAME, 'mg-snippet__agency').find_element(By.TAG_NAME, 'a').get_attribute('href')
                    content_lines.append(line)
                    content_source.append(source)
                    content_href_source.append(href_source)

                content_lines = '\n'.join(content_lines)
                content_source = '\n'.join(content_source)
                content_href_source = '\n'.join(content_href_source)

                # Добавление контента в базу данных, try на случай если в БД есть такой же объект
                try:
                    models.ContentNews.objects.create(line=content_lines,
                                                      source=content_source,
                                                      href_source=content_href_source,
                                                      title=title)
                except Exception:
                    pass

                try:
                    models.News.objects.create(title=title,
                                               created=time,
                                               photo_url=photo,
                                               avtor_id=models.AvtorNews.objects.get(avtor=avtor).id,
                                               content_id=models.ContentNews.objects.get(title=title).id,
                                               status=1)
                except Exception:
                    pass

                driver.close()
                driver.switch_to.window(driver.window_handles[0])

            except Exception:
                pass

        print('Обновление БД завершена!')
        driver.quit()
