from django.urls import path
import news.views

urlpatterns = [
    path('news/', news.views.main, name='main'),
    path('news/<int:pk>/', news.views.NewsDetail, name='news_detail'),
    path('about/', news.views.about, name='about'),
    path('register/', news.views.RegisterUser.as_view(), name='register'),
    path('login/', news.views.LoginUser.as_view(), name='login'),
    path('logout/', news.views.logout_user, name='logout'),
]
