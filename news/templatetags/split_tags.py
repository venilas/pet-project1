from django import template
from news.models import *

register = template.Library()


@register.filter(name='split')
def split(string, sep):
    return string.split('\n')
