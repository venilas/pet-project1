from django.contrib import admin
from news import models


@admin.register(models.News)
class News(admin.ModelAdmin):
    list_display = ('id', 'title', 'avtor', 'created', 'status', 'region')
    list_display_links = ('id', 'title')
    list_editable = ('status',)
    # list_filter = ('status', 'region')
    search_fields = ('title',)
    ordering = ('-created',)


@admin.register(models.AvtorNews)
class AvtorNews(admin.ModelAdmin):
    list_display = ('avtor',)


@admin.register(models.ContentNews)
class ContentNews(admin.ModelAdmin):
    list_display = ('title',)
