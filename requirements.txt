pip==22.3
django==4.1.2
Pillow==9.2.0
ipython

selenium==4.5.0
tqdm==4.64.1