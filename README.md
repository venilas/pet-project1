<h1>Новостной сайт</h1>

Данный проект представляет из-за себя новостной сайт. Данные для баз данных берёт из другой платформы новостей ЯндексДзен.

> Учитывайте версию Chrome и версию ChromeDriver
> 
> Все манипуляции проходят на версиях Chrome: 107.0.5304.87, ChromeDriver: 107.0.5304.62
> 
> Все версии ChromeDriver можете посмотреть и скачать [здесь](https://chromedriver.chromium.org/downloads)

<h4>Предстоящее задачи которые хотелось бы реализовать</h4>
- Оптимизировать код, а также проделать Code Review
- Обыграть разный исход событий
  - Если по какой-либо причине не удалось добавить контент к новости(потеря интернета), удалить объект из БД или пытаться заново получить контент из 
    парсинга
- Поменять Field для времени публикации
- Добавить путь если страница не найдена
- Показ новостей исходя от местоположения пользователя, если не удалось, то показать новости Уфы
- Добавить в navbar название региона который на данный момент находится пользователь
- Дать выбор пользователям выбор показа новостей по регионам
- Обыграть ситуацию если в блоке с новостями нет опубликованных новостей
- Оформление сайта, Front-end часть
- Реализовать добавления новости от пользователя

<h4>Минусы проекта</h4>
- Долгое добавление контента
- Нет мониторинга появлений новых новостей и добавления их в базу
- Низкий функционал сайта
- Оформление сайта хромает



__Команда для добавления новостей в БД__
> python manage.py scraper